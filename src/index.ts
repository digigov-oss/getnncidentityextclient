import soapClient from "./soapClient.js";
import {
  generateAuditRecord,
  AuditRecord,
  FileEngine,
  AuditEngine,
} from "@digigov-oss/gsis-audit-record-db";
import config from "./config.json";

export declare type AuditInit = AuditRecord;
export declare type GetNncIdentityExtOutputRecord = {
  countryCode?: string;
  countryDescr?: string;
  addressStreet?: string;
  addressNumber?: string;
  addressCity?: string;
  addressZipCode?: string;
  region?: string;
  regionalUnit?: string;
  municipality?: string;
  municipalUnit?: string;
  commune?: string;
  locality?: string;
  telephone?: string;
  countryCode2?: string;
  countryDescr2?: string;
  addressStreet2?: string;
  addressNumber2?: string;
  addressCity2?: string;
  addressZipCode2?: string;
  region2?: string;
  regionalUnit2?: string;
  municipality2?: string;
  municipalUnit2?: string;
  commune2?: string;
  locality2?: string;
  telephone2?: string;
  mobile?: string;
  email?: string;
  epikForeisFlag?: string;
  epidForeisFlag?: string;
  message?: string;
};

export type ErrorRecord = {
  errorCode: string;
  errorDescr: string;
};

/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export declare type Overrides = {
  endpoint?: string;
  prod?: boolean;
  auditInit?: AuditRecord;
  auditStoragePath?: string;
  auditEngine?: AuditEngine;
};

/**
 *
 * @param afm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
export const getIdentityExt = async (
  afm: string,
  user: string,
  pass: string,
  overrides?: Overrides
) => {
  const endpoint = overrides?.endpoint ?? "";
  const prod = overrides?.prod ?? false;
  const auditInit = overrides?.auditInit ?? ({} as AuditRecord);
  const auditStoragePath = overrides?.auditStoragePath ?? "/tmp";
  const auditEngine =
    overrides?.auditEngine ?? new FileEngine(auditStoragePath);
  const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
  const auditRecord = await generateAuditRecord(auditInit, auditEngine);
  if (!auditRecord) throw new Error("Audit record is not initialized");

  try {
    const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
    const response: GetNncIdentityExtOutputRecord | ErrorRecord =
      await s.getIdentityExt(afm);
    return { ...response, ...auditRecord };
  } catch (error) {
    throw error;
  }
};

export default getIdentityExt;
