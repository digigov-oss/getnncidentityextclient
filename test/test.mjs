import getIdentity from '../dist/esm/index.js';
import config from './config.json'; 
const test = async () => {
    try {
        const Identity = await getIdentity("052704062", config.user, config.pass);
        return Identity;
    } catch (error) {
        console.log(error);
    }
}

test().then((identity) => { console.log('ExtOutputRecord',identity); });