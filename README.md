# Client for getNncIdentityExt service of KED

Client to connect EMEP service, useful for nextjs/nodejs projects.

#### Example:
```
import getIdentityExt from '@digigov-oss/get-nnc-identity-ext-client';

const test = async () => {
   const overrides = {
        prod:false,
        auditInit: {
            auditUnit: 'grnet.gr',
        },
        auditStoragePath: '/auditStorage',
    }

    try {
        const Identity = await getIdentityExt("052704062", "username", "password",overrides);
         return Identity;
        } catch (error) {
        console.log(error);
        }
}

test().then((identity) => { console.log('getNncIdentityExtOutputRecord',identity); });
```
* you can use `overrides` to override the default values
* for your tests, you don't need to use the `overrides` mecahnism,in that case, the default storage path will be used ie `/tmp`
* look at [KED](https://www.gsis.gr/dimosia-dioikisi/ked/) standard guides for records you can use on auditInit"
Also, you can use `overrides` to override the default storage engine.
```
import getIdentityExt from '@digigov-oss/get-nnc-identity-Ext-client';
import {PostgreSqlEngine} from '@digigov-oss/gsis-audit-record-db'; 
const test = async () => {
    try {
        const overrides = {
        auditEngine: new PostgreSqlEngine('postgres://postgres:postgres@localhost:5432/postgres'),
        auditInit: {
            auditUnit: 'grnet.gr',
        },
        }
        const Identity = await getIdentityExt("052704062", config.user, config.pass, overrides);
        return Identity;
    } catch (error) {
        console.log(error);
    }
}

test().then((identity) => { console.log('getNncIdentityExtOutputRecord',identity); });

```

Look at module [AuditRecordDB](https://gitlab.grnet.gr/digigov-oss/auditRecordDB/-/blob/main/README.md) for more details on how to use the AuditEngine.

If you plan to use only the `FileEngine`, you can skip the installation of other engines by ignoring optional dependencies.
i.e.` yarn install --ignore-optional`

#### Returns
an object like the following:
```
{
  countryCode: 'GRC',
  countryDescr: 'Ελλάδα',
  addressStreet: 'Βενιζέλου Ελευθερίου',
  addressNumber: '86',
  addressCity: 'Αθήνα',
  telephone: '2101234567',
  countryCode2: 'GRC',
  countryDescr2: 'Ελλάδα',
  addressStreet2: 'Ιάσονος',
  addressNumber2: '14',
  addressCity2: 'Αθήνα',
  mobile: '6979114217',
  email: 'andreas-hal@hotmail.com',
  epikForeisFlag: '1',
  epidForeisFlag: '1',
  auditUnit: 'gov.gr',
  auditTransactionId: '52',
  auditProtocol: '1/2022-02-11',
  auditTransactionDate: '2022-02-11T08:39:50Z',
  auditUserIp: '127.0.0.1',
  auditUserId: 'system'  
}
```
or an error message like:
```
{ message: 'Ο Α.Φ.Μ. δεν είναι καταχωρημένος' }
```

#### Available AFM for testing:
019285930,  052704062,  076509078 

##### * Notes
you have to ask KED for notificationCenter_v_0.95 documentation to get more info about the output and error fields.

#### * known issues
KED advertises a wrong endpoint(!) for the `getNncIdentityExt` service on production WSDL. So, you have to use (override) the endpoint: `https://ked.gsis.gr/esb/notificationCenterElementsService`
You can do that by setting the `endpoint` property on the `overrides` object.

```
const overrides = {
    endpoint: 'https://ked.gsis.gr/esb/notificationCenterElementsService',
}
```
