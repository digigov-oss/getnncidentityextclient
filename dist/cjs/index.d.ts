import { AuditRecord, AuditEngine } from "@digigov-oss/gsis-audit-record-db";
export declare type AuditInit = AuditRecord;
export declare type GetNncIdentityExtOutputRecord = {
    countryCode?: string;
    countryDescr?: string;
    addressStreet?: string;
    addressNumber?: string;
    addressCity?: string;
    addressZipCode?: string;
    region?: string;
    regionalUnit?: string;
    municipality?: string;
    municipalUnit?: string;
    commune?: string;
    locality?: string;
    telephone?: string;
    countryCode2?: string;
    countryDescr2?: string;
    addressStreet2?: string;
    addressNumber2?: string;
    addressCity2?: string;
    addressZipCode2?: string;
    region2?: string;
    regionalUnit2?: string;
    municipality2?: string;
    municipalUnit2?: string;
    commune2?: string;
    locality2?: string;
    telephone2?: string;
    mobile?: string;
    email?: string;
    epikForeisFlag?: string;
    epidForeisFlag?: string;
    message?: string;
};
export declare type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export declare type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 *
 * @param afm string;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns AuditRecord | errorRecord
 */
export declare const getIdentityExt: (afm: string, user: string, pass: string, overrides?: Overrides | undefined) => Promise<{
    auditUnit?: string | undefined;
    auditTransactionId?: string | undefined;
    auditProtocol?: string | undefined;
    auditTransactionDate?: string | undefined;
    auditUserIp?: string | undefined;
    auditUserId?: string | undefined;
    countryCode?: string | undefined;
    countryDescr?: string | undefined;
    addressStreet?: string | undefined;
    addressNumber?: string | undefined;
    addressCity?: string | undefined;
    addressZipCode?: string | undefined;
    region?: string | undefined;
    regionalUnit?: string | undefined;
    municipality?: string | undefined;
    municipalUnit?: string | undefined;
    commune?: string | undefined;
    locality?: string | undefined;
    telephone?: string | undefined;
    countryCode2?: string | undefined;
    countryDescr2?: string | undefined;
    addressStreet2?: string | undefined;
    addressNumber2?: string | undefined;
    addressCity2?: string | undefined;
    addressZipCode2?: string | undefined;
    region2?: string | undefined;
    regionalUnit2?: string | undefined;
    municipality2?: string | undefined;
    municipalUnit2?: string | undefined;
    commune2?: string | undefined;
    locality2?: string | undefined;
    telephone2?: string | undefined;
    mobile?: string | undefined;
    email?: string | undefined;
    epikForeisFlag?: string | undefined;
    epidForeisFlag?: string | undefined;
    message?: string | undefined;
} | {
    auditUnit?: string | undefined;
    auditTransactionId?: string | undefined;
    auditProtocol?: string | undefined;
    auditTransactionDate?: string | undefined;
    auditUserIp?: string | undefined;
    auditUserId?: string | undefined;
    errorCode: string;
    errorDescr: string;
}>;
export default getIdentityExt;
